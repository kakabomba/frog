#!/usr/bin/env bash

docker run --rm --net=host -e POSTGRES_PASSWORD=riddle -e POSTGRES_USER=stork -e POSTGRES_DB=ladybug --name frog_db_instance frog_db
