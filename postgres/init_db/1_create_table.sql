CREATE SEQUENCE some_table_id_seq;

CREATE TABLE "some_table" (
    id integer NOT NULL DEFAULT nextval('some_table_id_seq'),
    value varchar(100),
    t TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

ALTER SEQUENCE some_table_id_seq OWNED BY some_table.id;
