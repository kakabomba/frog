#!/usr/bin/env bash

set -e

for ((i=0;i<=99;i++)); do
  echo "INSERT INTO some_table (value) VALUES('Row $i');" | psql -v --username "$POSTGRES_USER" --dbname "$POSTGRES_DB"
done
