from flask import Flask, jsonify
import os
import psycopg2

app = Flask(__name__)
app.cursor = psycopg2.connect(
        host=os.environ.get('POSTGRES_HOST'),
        dbname=os.environ.get('POSTGRES_DB'),
        user=os.environ.get('POSTGRES_USER'),
        password=os.environ.get('POSTGRES_PASSWORD')
    ).cursor()


@app.route('/')
def get_something():
    app.cursor.execute('SELECT * FROM "some_table" ORDER BY random() LIMIT 1')
    return jsonify(app.cursor.fetchone())


if __name__ == '__main__':
    app.run(debug=True, port=int(os.environ.get("PORT", 8080)))
