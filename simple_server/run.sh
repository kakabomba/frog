#!/usr/bin/env bash

docker run --rm --net=host -e POSTGRES_PASSWORD=riddle -e POSTGRES_USER=stork -e POSTGRES_DB=ladybug -e POSTGRES_HOST=localhost -e PORT=8080 --name frog_server_instance frog_server
